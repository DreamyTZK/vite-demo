## 预处理器
### sass

直接安装`sass`即可

```bash
yarn add sass -D
```

### less

```bash
yarn add less -D
```

## 组件样式库

### ElementUI-plus按需引入

1. 安装`vite`插件

   ```bash
   yarn add vite-plugin-style-import -D
   ```

2. 修改`vite.config.js`

   {% folding scss版本 %}

   此方式需要安装`sass`依赖

   ```javascript
   import { defineConfig } from 'vite'
   import vue from '@vitejs/plugin-vue'
   // 按需导入ElementUI-Plus
   import styleImport from 'vite-plugin-style-import'
   
   export default defineConfig({
     plugins: [
       vue(),
       styleImport({
         libs: [
           {
             libraryName: 'element-plus',
             esModule: true,
             ensureStyleFile: true,
             resolveStyle: (name) => {
               name = name.slice(3)
               return `element-plus/packages/theme-chalk/src/${name}.scss`
             },
             resolveComponent: (name) => {
               return `element-plus/lib/${name}`
             }
           }
         ]
       })
     ]
   })
   ```

   {% endfolding %}

   {% folding css版本 %}

   此方式不需要依赖`sass`

   ```javascript
   import { defineConfig } from 'vite'
   import vue from '@vitejs/plugin-vue'
   import styleImport from 'vite-plugin-style-import'
   
   export default defineConfig({
     plugins: [
       vue(),
       styleImport({
         libs: [
           {
             libraryName: 'element-plus',
             esModule: true,
             ensureStyleFile: true,
             resolveStyle: (name) => {
               return `element-plus/lib/theme-chalk/${name}.css`;
             },
             resolveComponent: (name) => {
               return `element-plus/lib/${name}`;
             },
           }
         ]
       })
     ]
   })
   ```

   {% endfolding %}

3. 使用组件——在`main.js`文件中引入对应组件即可

   ```javascript
   import { createApp } from 'vue'
   import App from './App.vue'
   
   // 按需导入Element-Plus组件
   import { ElButton } from 'element-plus'
   // 如果要使用.scss样式文件，则需要引入base.scss文件
   import 'element-plus/packages/theme-chalk/src/base.scss'
   const app = createApp(App)
   
   /**
    * 注册Element-Plus组件
    * 引入列表参考:
    * https://element-plus.gitee.io/#/zh-CN/component/quickstart
    * https://github.com/element-plus/element-plus/tree/dev/packages
    */
   // Element默认全局配置
   app.config.globalProperties.$ELEMENT = {
     // 可设置medium small mini
     // size: 'small',
     // zIndex: 3000
   }
   // Element组件
   const components = [ElButton]
   // 注册组件
   components.forEach((component) => {
     app.component(component.name, component)
   })
   // 挂载
   app.mount('#app')
   ```

   

### Vant

1. 安装

   ```bash
   yarn add vant@next
   ```

2. 编辑`vite.config.js`文件配置按需导入

   ```javascript
   export default defineConfig({
     plugins: [
       vue(),
       styleImport({
         libs: [
           {
             libraryName: 'vant',
             esModule: true,
             resolveStyle: (name) => `vant/es/${name}/style`
           }
         ]
       })
     ]
   })
   ```

3. 使用即可

## vue核心插件

### vue-router

1. 安装

   因为vue使用的是vue3版本，因此安装vue-router需要指定为4版本

   ```bash
   yarn add vue-router@4
   ```

2. 配置路由

   编辑`/router/index.js`

   ```javascript
   /**
    * @description: 路由配置文件
    * @author: 小康
    * @url: https://xiaokang.me
    * @Date: 2021-05-16 09:42:24
    * @LastEditTime: 2021-05-16 09:42:24
    * @LastEditors: 小康
    */
   
   import { createRouter, createWebHashHistory } from 'vue-router'
   const router = new createRouter({
     // 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
     history: createWebHashHistory(),
     routes: [
       {
         path: '/',
         redirect: '/home'
       },
       {
         path: '/home',
         component: () => import('../view/Home.vue')
       },
       {
         path: '/about',
         component: () => import('../view/About.vue')
       }
     ]
   })
   router.beforeEach((to, from, next) => {
     console.log(to, from, 111)
     next()
   })
   export default router
   
   ```

3. 在入口文件引入

   ```javascript
   import router from './router/index.js'
   const app = createApp(App)
   app.use(router)
   app.mount('#app')
   ```

4. 在组建中使用

   ```vue
   <template>
     <img alt="Vue logo" src="./assets/logo.png" />
     <div class="nav">
       <!-- url会显示 -->
       <el-button @click="routingGo({ name: 'home', query: { a: 1 } })"
         >Go to Home</el-button
       >
       <!-- url不会显示 -->
       <el-button @click="routingGo({ name: 'about', params: { a: 1 } })"
         >Go to About</el-button
       >
     </div>
     <router-view></router-view>
   </template>
   
   <script setup>
   // This starter template is using Vue 3 experimental <script setup> SFCs
   // Check out https://github.com/vuejs/rfcs/blob/script-setup-2/active-rfcs/0000-script-setup.md
   
   import { useRoute, useRouter } from 'vue-router';
   const router = useRouter();
   
   const routingGo = (obj) => {
     router.push(obj);
   };
   </script>
   
   <style>
   </style>
   ```

5. 在组件中使用路由传递的参数

   ```vue
   <script setup>
     import { useRouter, useRoute } from 'vue-router';
     const route = useRoute();
     console.log(route.query);
     console.log(route.params, 'params');
   </script>
   ```

   

### vuex

1. 安装

   ```bash
   yarn add vuex@next --save
   ```

2. 新建`/store/index.js`

   ```javascript
   /**
    * @description: vuex状态管理
    * @author: 小康
    * @url: https://xiaokang.me
    * @Date: 2021-05-16 12:05:14
    * @LastEditTime: 2021-05-16 12:05:14
    * @LastEditors: 小康
    */
   import { createStore } from 'vuex'
   
   export default createStore({
     state() {
       return {
         count: 0
       }
     },
     mutations: {
       increment(state) {
         state.count++
       }
     },
     actions: {
       increment(context) {
         context.commit('increment')
       }
     }
   })
   
   ```

3. `main.js`挂载

   ```javascript
   import store from './store/index.js'
   const app = createApp(App)
   app.use(store)
   app.mount('#app')
   ```

4. 在组件中使用

   ```vue
   <!--
    * @description: 
    * @author: 小康
    * @url: https://xiaokang.me
    * @Date: 2021-05-16 09:43:33
    * @LastEditTime: 2021-05-16 15:18:57
    * @LastEditors: 小康
   -->
   <template>
     <p>当前vuex中count的值为:{{ store.state.count }}</p>
   </template>
   
   <script setup>
   import { useStore } from 'vuex';
   const store = useStore();
   store.commit('increment');
   </script>
   
   ```

   

## vite插件

### vite-plugin-html

1. 安装

   ```bash
   yarn add vite-plugin-html -D
   ```

2. 在html文件中使用

   ```html
   <title><%- title %></title>
   ```

3. `vite.config.js`配置（按需引入）

   ```javascript
   ```

   

   

