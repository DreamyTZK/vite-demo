/**
 * @description: postcss配置
 * @author: 小康
 * @url: https://xiaokang.me
 * @Date: 2021-05-16 17:33:21
 * @LastEditTime: 2021-05-16 17:33:21
 * @LastEditors: 小康
 */
module.exports = {
  plugins: {
    autoprefixer: {
      overrideBrowserslist: [
        'Android 4.4.4',
        'iOS 10',
        'Chrome > 60',
        'ff > 60',
        // 'ie >= 8',
        'last 10 versions' // 所有主流浏览器最近10版本用
      ],
      grid: true
    },
    'postcss-pxtorem': {
      // 37.5或75 使用vant推荐设置为37.5
      rootValue: 37.5,
      // 是一个存储哪些将被转换的属性列表，这里设置为['*']全部，假设需要仅对边框进行设置，可以写['*', '!border*']
      propList: ['*'],
      // 保留rem小数点多少位
      unitPrecision: 7,
      // 媒体查询( @media screen 之类的)中不生效
      mediaQuery: false,
      // px小于12的不会被转换
      minPixelValue: 12,
      // 则是一个对css选择器进行过滤的数组，比如你设置为['fs']，那例如fs-xl类名，里面有关px的样式将不被转换，这里也支持正则写法。
      selectorBlackList: ['.radius']
    }
  }
}
