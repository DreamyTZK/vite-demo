import { resolve } from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import legacy from '@vitejs/plugin-legacy'
import alias from '@rollup/plugin-alias'
import path from 'path'
// 按需导入处理
import styleImport from 'vite-plugin-style-import'
import { minifyHtml, injectHtml } from 'vite-plugin-html'
export default defineConfig({
  // 项目基路径
  base: '/',

  plugins: [
    alias(),
    vue(),
    styleImport({
      libs: [
        // element-plus
        {
          libraryName: 'element-plus',
          esModule: true,
          ensureStyleFile: true,
          resolveStyle: (name) => {
            name = name.slice(3)
            return `element-plus/packages/theme-chalk/src/${name}.scss`
          },
          resolveComponent: (name) => {
            return `element-plus/lib/${name}`
          }
        },
        {
          libraryName: 'vant',
          esModule: true,
          resolveStyle: (name) => `vant/es/${name}/style`
        }
      ]
    }),
    minifyHtml({
      collapseBooleanAttributes: true,
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
      minifyURLs: true,
      removeAttributeQuotes: true,
      removeComments: true,
      removeEmptyAttributes: true
    }),
    injectHtml({
      injectData: {
        title: 'vite-html-demo'
      }
    }),
    legacy({
      targets: ['defaults', 'and_uc >= 10']
      // targets: ['defaults', 'ie >= 11'],
      // additionalLegacyPolyfills: ['regenerator-runtime/runtime']
    })
  ],
  server: {
    proxy: {
      '/api': {
        target: 'http://jsonplaceholder.typicode.com',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  },
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html'),
        pay: resolve(__dirname, 'pay.html')
      }
    },
    // es2015
    target: 'modules',
    // 输出目录
    outDir: 'dist',
    // 静态资源位置
    assetsDir: 'assets'
  },
  resolve: {
    alias: {
      '/@': path.resolve(__dirname, 'src')
    }
  }
})
