/**
 * @description: vuex状态管理
 * @author: 小康
 * @url: https://xiaokang.me
 * @Date: 2021-05-16 12:05:14
 * @LastEditTime: 2021-05-16 12:05:14
 * @LastEditors: 小康
 */
import { createStore } from 'vuex'

export default createStore({
  state() {
    return {
      count: 0
    }
  },
  mutations: {
    increment(state) {
      state.count++
    }
  },
  actions: {
    increment(context) {
      context.commit('increment')
    }
  }
})
