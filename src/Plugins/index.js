/**
 * @description: 引入插件入口文件
 * @author: 小康
 * @url: https://xiaokang.me
 * @Date: 2021-05-16 16:14:09
 * @LastEditTime: 2021-05-16 16:14:09
 * @LastEditors: 小康
 */

// 导入ElementPlus相关组件
import {
  components as ElementPlusComponents,
  plugins as ElementPlusPlugins
} from './ElementPlus'
// 导入Vant相关组件
import { components as VantComponents, plugins as VantPlugins } from './Vant'
const components = [...ElementPlusComponents, ...VantComponents]
const plugins = [...ElementPlusPlugins, ...VantPlugins]
export { components, plugins }
