/**
 * @description: 导入ElementPlus组件
 * @author: 小康
 * @url: https://xiaokang.me
 * @Date: 2021-05-16 16:15:02
 * @LastEditTime: 2021-05-16 16:15:02
 * @LastEditors: 小康
 */
/**
 * 注册Element-Plus组件
 * 引入列表参考:
 * https://element-plus.gitee.io/#/zh-CN/component/quickstart
 * https://github.com/element-plus/element-plus/tree/dev/packages
 */

// 如果要使用.scss样式文件，则需要引入base.scss文件
import 'element-plus/packages/theme-chalk/src/base.scss'

import { ElButton, ElMessage } from 'element-plus'

// 组件
const components = [ElButton]
// 扩展
const plugins = [ElMessage]

export { components, plugins }
