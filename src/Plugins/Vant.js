/**
 * @description:
 * @author: 小康
 * @url: https://xiaokang.me
 * @Date: 2021-05-16 16:58:36
 * @LastEditTime: 2021-05-16 16:58:36
 * @LastEditors: 小康
 */
/**
 * 注册Vant组件
 */

// 按需引入Vant组件
import { Button } from 'vant'

// 组件
const components = [Button]
// 扩展
const plugins = []

export { components, plugins }
