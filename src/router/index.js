/**
 * @description: 路由配置文件
 * @author: 小康
 * @url: https://xiaokang.me
 * @Date: 2021-05-16 09:42:24
 * @LastEditTime: 2021-05-16 09:42:24
 * @LastEditors: 小康
 */

import { createRouter, createWebHashHistory } from 'vue-router'
const router = new createRouter({
  // 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('/@/view/Home.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('/@/view/About.vue')
    },
    {
      path: '/vant',
      name: 'vant',
      component: () => import('/@/view/VantDemo.vue')
    }
  ]
})
router.beforeEach((to, from, next) => {
  // console.log(to, from, 111)
  next()
})
export default router
