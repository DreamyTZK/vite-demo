/**
 * @description: pay页面
 * @author: 小康
 * @url: https://xiaokang.me
 * @Date: 2021-05-16 23:17:47
 * @LastEditTime: 2021-05-16 23:17:47
 * @LastEditors: 小康
 */
// import 'amfe-flexible/index.js'
import { createApp } from 'vue'
import App from './Pay.vue'

const app = createApp(App)

app.mount('#pay')
