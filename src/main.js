import 'amfe-flexible/index.js'
import { createApp } from 'vue'
import App from './App.vue'

import router from './router/index.js'
import store from './store/index.js'

import { components, plugins } from './Plugins/index.js'

const app = createApp(App)

// Element默认全局配置
app.config.globalProperties.$ELEMENT = {
  // 可设置medium small mini
  // size: 'small',
  // zIndex: 3000
}

// 注册组件
components.forEach((component) => {
  app.component(component.name, component)
})
plugins.forEach((plugin) => {
  app.use(plugin)
})
// 挂载
app.use(router)
app.use(store)
app.mount('#app')
